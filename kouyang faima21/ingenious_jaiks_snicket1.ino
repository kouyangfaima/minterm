// Pin definitions
const int redPin = 9;
const int greenPin = 10;
const int bluePin = 11;

void setup() {
  // Initialize the LED pins as outputs
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  // Start the serial communication at 9600 baud
  Serial.begin(9600);
  Serial.println("Enter a command (r, g, b, on, off):");
}

void loop() {
  // Check if data is available to read
  if (Serial.available() > 0) {
    // Read the incoming byte
    String command = Serial.readStringUntil('\n');
    command.trim();  // Remove any leading/trailing whitespace

    // Handle the command
    if (command.equalsIgnoreCase("r")) {
      setColor(255, 0, 0); // Red
      Serial.println("Red light on");
    } else if (command.equalsIgnoreCase("g")) {
      setColor(0, 255, 0); // Green
      Serial.println("Green light on");
    } else if (command.equalsIgnoreCase("b")) {
      setColor(0, 0, 255); // Blue
      Serial.println("Blue light on");
    } else if (command.equalsIgnoreCase("on")) {
      setColor(255, 255, 255); // White
      Serial.println("White light on");
    } else if (command.equalsIgnoreCase("off")) {
      setColor(0, 0, 0); // Turn off
      Serial.println("Lights off");
    } else {
      Serial.println("Unknown command");
    }
  }
}

// Function to set the color of the RGB LED
void setColor(int red, int green, int blue) {
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}
